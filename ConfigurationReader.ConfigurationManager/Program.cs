﻿using ConfigurationReader.Data;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using StackExchange.Redis;
using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConfigurationReader.ConfigurationManager
{
    class Program
    {
        private static Lazy<ConnectionMultiplexer> redis = new Lazy<ConnectionMultiplexer>(() =>
        {
            return ConnectionMultiplexer.Connect("172.31.19.198");
        });

        
        static void Main(string[] args)
        {
            ConfigurationRepository _mongoDbRepo = new ConfigurationRepository("mongodb://172.31.19.198:27017");
            var factory = new ConnectionFactory() { HostName = "172.31.19.198" };
            IDatabase cache = redis.Value.GetDatabase();

            Task.Factory.StartNew(() => {
                using (var connection = factory.CreateConnection())
                using (var channel = connection.CreateModel())
                {

                    var consumer = new EventingBasicConsumer(channel);
                    consumer.Received += (model, ea) =>
                    {
                        var allkeys = _mongoDbRepo.GetAllConfigurationValues().GetAwaiter().GetResult();
                        foreach (var item in allkeys)
                        {
                            cache.HashSet(item.Name, new HashEntry[] { new HashEntry("Type", item.Type), new HashEntry("Value", item.Value) });
                            Console.WriteLine("Cache Updated");
                        }
                    };
                    channel.BasicConsume(queue: "boyner",
                                         autoAck: true,
                                         consumer: consumer);

                    Console.WriteLine(" Press [enter] to exit.");
                    Console.ReadLine();
                }

            });

            Console.ReadLine();
            Console.ReadLine();

        }
    }
}
