﻿using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Text;

namespace ConfigurationReader
{
    public class ConfiturationReader
    {
        private static string _connectionString;
        private static string _applicationName;

        private static Lazy<ConnectionMultiplexer> redis = new Lazy<ConnectionMultiplexer>(() =>
        {
            return ConnectionMultiplexer.Connect(_connectionString);
        });

        IDatabase cache;

        public ConfiturationReader(string applicationName, string connectionString)
        {
            _connectionString = connectionString;
            _applicationName = applicationName;
            cache = redis.Value.GetDatabase();
        }

        public T GetValue<T>(string key)
        {
            if (cache.HashExists(key, "Value"))
            {
                if(cache.HashGet(key, "IsActive") != "false")
                {
                     return (T)Convert.ChangeType(cache.HashGet(key, "Value"), typeof(T));
                }
                throw new InvalidOperationException("Key is not active");
            }

            throw new InvalidOperationException("Key not found");
        }
    }
}
