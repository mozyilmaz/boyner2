﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using BoynerTestCase.Client.Models;
using ConfigurationReader;

namespace BoynerTestCase.Client.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public IActionResult GetKey(string key)
        {
            ConfiturationReader c = new ConfiturationReader("BoynerClientApp", "172.31.19.198");
            return Content(c.GetValue<string>(key));
        }

 
    }
}
