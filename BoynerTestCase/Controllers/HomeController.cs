﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using BoynerTestCase.Models;
using ConfigurationReader.Data;
using RabbitMQ.Client;
using System.Text;
using RabbitMQ.Client.Events;
using StackExchange.Redis;

namespace BoynerTestCase.Controllers
{

    public class HomeController : Controller
    {
        static ConfigurationRepository _mongoDbRepo = new ConfigurationRepository("mongodb://172.31.19.198:27017");
        private static Lazy<ConnectionMultiplexer> redis = new Lazy<ConnectionMultiplexer>(() =>
        {
            return ConnectionMultiplexer.Connect("172.31.19.198");
        });

        public HomeController()
        {
            var factory = new ConnectionFactory() { HostName = "172.31.19.198" };
            using (var connection = factory.CreateConnection())
            {
                using (var channel = connection.CreateModel())
                {
                    channel.QueueDeclare(queue: "boyner",
                                 durable: false,
                                 exclusive: false,
                                 autoDelete: false,
                                 arguments: null);

                }
            }
        }

        public IActionResult Index()
        {

            return View();
        }
 
        public IActionResult ConfigurationValues()
        {
            var allkeys = _mongoDbRepo.GetAllConfigurationValues().GetAwaiter().GetResult();
            return View(allkeys);
        }
        [HttpGet]
        public IActionResult CreateConfigurationValue()
        {
            return View(new ConfigurationValue());
        }
        public async Task<IActionResult> Delete(Guid id)
        {
            await _mongoDbRepo.DeleteConfigurationValue(id);
            return RedirectToAction("ConfigurationValues");
        }

        [HttpGet]
        public async Task<IActionResult> EditConfigurationValue(Guid id)
        {
            var confValue = await _mongoDbRepo.GetConfigurationValueById(id);
            return View(confValue);
        }

        [HttpPost]
        public async Task<IActionResult> EditConfigurationValue(ConfigurationValue editedValue)
        {
            var confValue = await _mongoDbRepo.UpdateConfigurationValue(editedValue);
            SendRefreshCacheMessage();
            return RedirectToAction("ConfigurationValues");
        }

        [HttpPost]
        public IActionResult PostCreateConfigurationValue(ConfigurationValue newConfigValue)
        {
            _mongoDbRepo.InsertConfigurationValue(newConfigValue).GetAwaiter().GetResult();

            SendRefreshCacheMessage();
            return RedirectToAction("ConfigurationValues");
        }

        private static void SendRefreshCacheMessage()
        {
            var factory = new ConnectionFactory() { HostName = "172.31.19.198" };
            using (var connection = factory.CreateConnection())
            {
                using (var channel = connection.CreateModel())
                {
                    string message = "ReloadCache";
                    var body = Encoding.UTF8.GetBytes(message);

                    channel.BasicPublish(exchange: "",
                                         routingKey: "boyner",
                                         basicProperties: null,
                                         body: body);
                }
            }
        }
    }
}
